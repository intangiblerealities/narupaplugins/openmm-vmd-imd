/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2016 Stanford University and the Authors.           *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

#include "OpenCLImdKernels.h"
#include "OpenCLImdKernelSources.h"
#include "openmm/NonbondedForce.h"
#include "openmm/internal/ContextImpl.h"
#include "openmm/opencl/OpenCLBondedUtilities.h"
#include "openmm/opencl/OpenCLForceInfo.h"
#include <cstring>
#include <map>

using namespace ImdPlugin;
using namespace OpenMM;
using namespace std;

class OpenCLCalcImdForceKernel::StartCalculationPreComputation : public OpenCLContext::ForcePreComputation {
public:
    StartCalculationPreComputation(OpenCLCalcImdForceKernel& owner) : owner(owner) {
    }
    void computeForceAndEnergy(bool includeForces, bool includeEnergy, int groups) {
        owner.beginComputation(includeForces, includeEnergy, groups);
    }
    OpenCLCalcImdForceKernel& owner;
};

class OpenCLCalcImdForceKernel::ExecuteTask : public OpenCLContext::WorkTask {
public:
    ExecuteTask(OpenCLCalcImdForceKernel& owner) : owner(owner) {
    }
    void execute() {
        owner.executeOnWorkerThread();
    }
    OpenCLCalcImdForceKernel& owner;
};

class OpenCLCalcImdForceKernel::AddForcesPostComputation : public OpenCLContext::ForcePostComputation {
public:
    AddForcesPostComputation(OpenCLCalcImdForceKernel& owner) : owner(owner) {
    }
    double computeForceAndEnergy(bool includeForces, bool includeEnergy, int groups) {
        return owner.addForces(includeForces, includeEnergy, groups);
    }
    OpenCLCalcImdForceKernel& owner;
};


OpenCLCalcImdForceKernel::~OpenCLCalcImdForceKernel() {
    if (imdForces != NULL)
        delete imdForces;
}

void OpenCLCalcImdForceKernel::initialize(const System& system, const ImdForce& force) {

    initializeImd(system, force);

    queue = cl::CommandQueue(cl.getContext(), cl.getDevice());
    int elementSize = (cl.getUseDoublePrecision() ? sizeof(double) : sizeof(float));
    imdForces = new OpenCLArray(cl, 3*system.getNumParticles(), elementSize, "imdForces");

    map<string, string> defines;
    defines["NUM_ATOMS"] = cl.intToString(cl.getNumAtoms());
    defines["PADDED_NUM_ATOMS"] = cl.intToString(cl.getPaddedNumAtoms());
    defines["FORCE_CONVERSION"] = cl.doubleToString((double)forceConversion);

    cl::Program program = cl.createProgram(OpenCLImdKernelSources::imdForce, defines);
    addForcesKernel = cl::Kernel(program, "addForces");
    forceGroupFlag = (1<<force.getForceGroup());
    cl.addPreComputation(new StartCalculationPreComputation(*this));
    cl.addPostComputation(new AddForcesPostComputation(*this));

}

double OpenCLCalcImdForceKernel::execute(ContextImpl& context, bool includeForces, bool includeEnergy) {
    // This method does nothing.  The actual calculation is started by the pre-computation, continued on
    // the worker thread, and finished by the post-computation.
    
    return 0;
}

void OpenCLCalcImdForceKernel::beginComputation(bool includeForces, bool includeEnergy, int groups) {
    if ((groups&forceGroupFlag) == 0)
        return;
    contextImpl.getPositions(openmmPositions);
    
    // The actual force computation will be done on a different thread.
    
    cl.getWorkThread().addTask(new ExecuteTask(*this));
}

void OpenCLCalcImdForceKernel::executeOnWorkerThread() {
    // Configure the IMD interface object.
    if(!imd.isClientConnected())
        imd.tryConnect(waitForConnection);

    int numParticles = contextImpl.getSystem().getNumParticles();
    int step = cl.getStepCount();

    if(step != lastStepIndex)
    {
        lastStepIndex = step;
        processImdInput();
    }

    if(lastStepIndex % rate == 0)
    {
        updatePositionsToSend(openmmPositions);
        sendPositions();
        sendEnergies();
    }
    IF_DEBUG std::cerr << "Applying IMD forces" << std::endl;
    applyImdForces(numParticles);
}

void OpenCLCalcImdForceKernel::sendEnergies()
{
    IMDEnergies energies;
    energies.tstep = cl.getStepCount();
    imd.sendEnergies(energies);
}

void OpenCLCalcImdForceKernel::applyImdForces(int numParticles)
{
    perAtomForces.resize(3 * numParticles);
    memset(&perAtomForces[0], 0, 3*numParticles*sizeof(float));

    //TODO probably not worth parallelizing, but to consider.
    // OpenMM go to great lengths to order arrays nicely for GPUs
    // so scatter the received forces here rather than on the GPU.
    for(int i =0; i < receivedAtomIndices.size(); i++)
    {
        int atomIndex=receivedAtomIndices[i];
        perAtomForces[3*atomIndex] = receivedForces[3*i];
        perAtomForces[3*atomIndex+1] = receivedForces[3*i+1];
        perAtomForces[3*atomIndex+2] = receivedForces[3*i+2];

    }

    if (cl.getUseDoublePrecision()) {
        double* buffer = (double*) cl.getPinnedBuffer();
        for (int i = 0; i < numParticles; ++i) {
            buffer[3*i] = (double) perAtomForces[3*i];
            buffer[3*i+1] = (double) perAtomForces[3*i + 1];
            buffer[3*i+2] = (double) perAtomForces[3*i + 2];
        }
    }
    else {
        float* buffer = (float*) cl.getPinnedBuffer();
        for (int i = 0; i < numParticles; ++i) {
            buffer[3*i] = perAtomForces[3*i];
            buffer[3*i+1] = perAtomForces[3*i + 1];
            buffer[3*i+2] = perAtomForces[3*i + 2];
        }
    }
    queue.enqueueWriteBuffer(imdForces->getDeviceBuffer(), CL_TRUE, 0, imdForces->getSize()*imdForces->getElementSize(), cl.getPinnedBuffer(), NULL, &syncEvent);

}
double OpenCLCalcImdForceKernel::addForces(bool includeForces, bool includeEnergy, int groups) {
    if ((groups&forceGroupFlag) == 0)
        return 0;

    // Wait until executeOnWorkerThread() is finished.
    
    cl.getWorkThread().flush();
    vector<cl::Event> events(1);
    events[0] = syncEvent;
    syncEvent = cl::Event();
    queue.enqueueWaitForEvents(events);

    // Add in the forces.
    
    if (includeForces) {
        addForcesKernel.setArg<cl::Buffer>(0, imdForces->getDeviceBuffer());
        addForcesKernel.setArg<cl::Buffer>(1, cl.getForceBuffers().getDeviceBuffer());
        addForcesKernel.setArg<cl::Buffer>(2, cl.getAtomIndexArray().getDeviceBuffer());
        cl.executeKernel(addForcesKernel, cl.getNumAtoms());
    }
    
    //TODO IMD force produces no energy contribution...
    double energy = 0;
    return energy;
}
