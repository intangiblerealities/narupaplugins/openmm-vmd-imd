/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2016 Stanford University and the Authors.           *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

/**
 * This tests the CUDA implementation of ImdForce.
 */

#include "ImdForce.h"
#include "openmm/internal/AssertionUtilities.h"
#include "openmm/Context.h"
#include "openmm/CustomExternalForce.h"
#include "openmm/LangevinIntegrator.h"
#include "openmm/Platform.h"
#include "openmm/System.h"
#include <iostream>
#include <string>
#include <vector>

using namespace ImdPlugin;
using namespace OpenMM;
using namespace std;

extern "C" OPENMM_EXPORT void registerImdCudaKernelFactories();

void testForce() {
    // Create a System that applies a force based on the distance between two atoms.

    const int numParticles = 4;
    System system;
    vector<Vec3> positions(numParticles);
    for (int i = 0; i < numParticles; i++) {
        system.addParticle(1.0);
        positions[i] = Vec3(i, 0.1*i, -0.3*i);
    }
    string script =
        "d: DISTANCE ATOMS=1,3\n"
        "BIASVALUE ARG=d";
    ImdForce* imd = new ImdForce(script);
    system.addForce(imd);
    LangevinIntegrator integ(300.0, 1.0, 1.0);
    Platform& platform = Platform::getPlatformByName("CUDA");
    Context context(system, integ, platform);
    context.setPositions(positions);

    // Compute the forces and energy.

    State state = context.getState(State::Energy | State::Forces);
    Vec3 delta = positions[0]-positions[2];
    double dist = sqrt(delta.dot(delta));
    ASSERT_EQUAL_TOL(dist, state.getPotentialEnergy(), 1e-5);
    ASSERT_EQUAL_VEC(-delta/dist, state.getForces()[0], 1e-5);
    ASSERT_EQUAL_VEC(Vec3(), state.getForces()[1], 1e-5);
    ASSERT_EQUAL_VEC(delta/dist, state.getForces()[2], 1e-5);
    ASSERT_EQUAL_VEC(Vec3(), state.getForces()[3], 1e-5);
}

int main(int argc, char* argv[]) {
    try {
        registerImdCudaKernelFactories();
        if (argc > 1)
            Platform::getPlatformByName("CUDA").setPropertyDefaultValue("CudaPrecision", string(argv[1]));
        testForce();
    }
    catch(const std::exception& e) {
        std::cout << "exception: " << e.what() << std::endl;
        return 1;
    }
    std::cout << "Done" << std::endl;
    return 0;
}
