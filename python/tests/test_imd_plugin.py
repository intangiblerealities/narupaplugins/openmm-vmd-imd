# Copyright (c) Interactive Scientific LTD. All rights reserved.
# Licensed under the GPL. See License.txt in the project root for license information.
import time
from concurrent.futures.thread import ThreadPoolExecutor

import pytest

import numpy as np

import simtk.openmm as mm
from mock import Mock
from simtk.openmm import app
# Prefixed units in `simtk.unit` are added programmatically and are not
# recognized by pylint and PyCharm.
from simtk.unit import kelvin, picosecond, femtosecond, nanometer, angstrom, \
    kilocalories_per_mole  # pylint: disable=no-name-in-module
from pyvmdimd.imd_client import IMDClient
import openmmimd

IMPLEMENTED_PLATFORMS = ('Reference',)
IMD_FORCE_GROUP = 1
TEST_ADDRESS = 'localhost'
TEST_PORT = 9000
TEST_RATE = 1
TEST_MESSAGE_DELAY = 0.5

import socket
from contextlib import closing


def find_free_port():
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
        s.bind(('', 0))
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        return s.getsockname()[1]


@pytest.fixture
def basic_simulation():
    """
    Setup a minimal OpenMM simulation with two methane molecules.
    """
    # In this function, we define matrices and we want to align the column.
    # We disable the pylint warning about bad spacing for the scope of the
    # function.
    # pylint: disable=bad-whitespace
    periodic_box_vector = [
        [50, 0, 0],
        [0, 50, 0],
        [0, 0, 50]
    ]
    positions = np.array([
        # First residue
        [0, 0, 0],  # C
        [5.288, 1.610, 9.359],  # H
        [2.051, 8.240, -6.786],  # H
        [-10.685, -0.537, 1.921],  # H
        # Second residue, copied from the first but shifted
        # by 5 nm along the Z axis
        [0, 0, 5],  # C
        [5.288, 1.610, 14.359],  # H
        [2.051, 8.240, -1.786],  # H
        [-10.685, -0.537, 6.921],  # H
    ], dtype=np.float32)

    topology = app.Topology()
    carbon = app.Element.getBySymbol('C')
    hydrogen = app.Element.getBySymbol('H')
    chain = topology.addChain()
    residue = topology.addResidue(name='METH1', chain=chain)
    atom_c1 = topology.addAtom(element=carbon, name='C1', residue=residue)
    atom_h2 = topology.addAtom(element=hydrogen, name='H2', residue=residue)
    atom_h3 = topology.addAtom(element=hydrogen, name='H3', residue=residue)
    atom_h4 = topology.addAtom(element=hydrogen, name='H4', residue=residue)
    topology.addBond(atom_c1, atom_h2)
    topology.addBond(atom_c1, atom_h3)
    topology.addBond(atom_c1, atom_h4)
    chain = topology.addChain()
    residue = topology.addResidue(name='METH2', chain=chain)
    atom_c1 = topology.addAtom(element=carbon, name='C1', residue=residue)
    atom_h2 = topology.addAtom(element=hydrogen, name='H2', residue=residue)
    atom_h3 = topology.addAtom(element=hydrogen, name='H3', residue=residue)
    atom_h4 = topology.addAtom(element=hydrogen, name='H4', residue=residue)
    topology.addBond(atom_c1, atom_h2)
    topology.addBond(atom_c1, atom_h3)
    topology.addBond(atom_c1, atom_h4)

    system = mm.System()
    system.setDefaultPeriodicBoxVectors(*periodic_box_vector)
    system.addParticle(mass=12)
    system.addParticle(mass=1)
    system.addParticle(mass=1)
    system.addParticle(mass=1)
    system.addParticle(mass=12)
    system.addParticle(mass=1)
    system.addParticle(mass=1)
    system.addParticle(mass=1)

    force = mm.NonbondedForce()
    force.setNonbondedMethod(force.NoCutoff)
    # These non-bonded parameters are completely wrong, but it does not matter
    # for the tests as long as we do not start testing the dynamic and
    # thermodynamics properties of methane.
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    force.addParticle(charge=0, sigma=0.47, epsilon=3.5)
    system.addForce(force)

    integrator = mm.LangevinIntegrator(300 * kelvin, 1 / picosecond, 2 * femtosecond)

    simulation = app.Simulation(topology, system, integrator)
    simulation.context.setPeriodicBoxVectors(*periodic_box_vector)
    simulation.context.setPositions(positions * nanometer)

    return simulation


@pytest.fixture(params=IMPLEMENTED_PLATFORMS)
def simulation_and_port(request, basic_simulation):
    """
    Basic OpenMM simulation with the InteractiveForce attached.
    """

    platform_name = request.param
    platform = mm.Platform.getPlatformByName(platform_name)

    topology = basic_simulation.topology
    system = basic_simulation.system
    integrator = basic_simulation.integrator
    positions = basic_simulation.context.getState(getPositions=True).getPositions()

    # Integrators get attached to contexts. Once attached, an integrator cannot be attached to another context.
    # This is the best way I found to build a new identical integrator that can be attached to a new context.
    new_integrator = mm.XmlSerializer.deserialize(mm.XmlSerializer.serialize(integrator))

    port = find_free_port()
    force = openmmimd.ImdForce(TEST_ADDRESS, port, True, TEST_RATE)
    force.setForceGroup(IMD_FORCE_GROUP)
    system.addForce(force)

    simulation = app.Simulation(topology, system, new_integrator, platform=platform)
    simulation.context.setPositions(positions)
    return simulation, port


@pytest.fixture
def client_simulation(simulation_and_port):
    simulation, port = simulation_and_port
    client = IMDClient(TEST_ADDRESS, port)
    pool = ThreadPoolExecutor()
    task = pool.submit(client.loop)
    yield client, simulation
    client.kill_loop()


@pytest.mark.parametrize('force', (openmmimd.ImdForce,))
def test_instantiate_force(force):
    """
    Test that creating a force class does not cause an error.
    """
    force()


def test_simulation_run_imd(simulation_and_port):
    """
    Test that a simulation can run when an InteractiveForce is added.
    """
    assert simulation_and_port.currentStep == 0
    simulation_and_port.step(5)
    assert simulation_and_port.currentStep == 5


@pytest.mark.timeout(5)
def test_simulation_imd_receive_coordinates(client_simulation):
    client, simulation = client_simulation
    mock = Mock()
    client.set_coordinate_observer(mock.callback)

    simulation.step(1)
    time.sleep(TEST_MESSAGE_DELAY)
    assert client.connected
    assert mock.callback.call_count == 1


@pytest.mark.timeout(5)
def test_simulation_imd_correct_coordinates(client_simulation):
    """
    tests that the client receives the correct positions, in angstroms (vmd units).
    Note that OpenMM sends positions *before* they are integrated, so it's the positions one step
    behind openmm's current state.
    """
    client, simulation = client_simulation

    mock = Mock()

    client.set_coordinate_observer(mock)
    openmm_positions = simulation.context.getState(getPositions=True).getPositions(asNumpy=True).value_in_unit(angstrom)
    simulation.step(1)
    time.sleep(TEST_MESSAGE_DELAY)
    received_positions = client._coordinates
    assert np.allclose(received_positions, openmm_positions, rtol=1.e-5)


@pytest.mark.timeout(5)
def test_simulation_imd_receive_energies(client_simulation):
    client, simulation = client_simulation
    mock = Mock()
    client.set_energy_observer(mock.callback)
    simulation.step(1)
    time.sleep(TEST_MESSAGE_DELAY)
    assert mock.callback.call_count == 1


@pytest.mark.timeout(5)
def test_simulation_imd_apply_forces(client_simulation):
    client, simulation = client_simulation

    TEST_INDICES = np.array([0, 1, 2])
    TEST_FORCES = np.array([[0, 0, 1], [0, 1, 0], [2, 0, 1]], dtype=float)

    def get_forces(positions):
        indices = TEST_INDICES
        forces = TEST_FORCES
        return indices, forces

    client.set_force_provider(get_forces)
    simulation.step(1)
    time.sleep(TEST_MESSAGE_DELAY)
    imd_forces = simulation.context.getState(getForces=True, groups=IMD_FORCE_GROUP).getForces(
        asNumpy=True).value_in_unit(kilocalories_per_mole / angstrom)

    assert np.allclose(imd_forces[TEST_INDICES], TEST_FORCES)


@pytest.mark.timeout(5)
def test_imd_set_rate(client_simulation):
    client, simulation = client_simulation
    mock = Mock()
    energyMock = Mock()
    client.set_coordinate_observer(mock.callback)
    client.set_energy_observer(energyMock.callback)

    new_rate = 10
    expected_messages = 3

    client.set_transmission_rate(new_rate)
    simulation.step(new_rate * expected_messages)
    time.sleep(TEST_MESSAGE_DELAY)
    assert mock.callback.call_count == expected_messages
    assert energyMock.callback.call_count == expected_messages
