%define DOCSTRING
"Module providing an IMD force, implementing the VMD IMD API enabling "
"interactive molecular dynamics."
%enddef

%module(docstring=DOCSTRING) openmmimd

#undef __cplusplus
#define __cplusplus 201402L
%import(module="simtk.openmm") "swig/OpenMMSwigHeaders.i"
%include "swig/typemaps.i"
%include "std_string.i"

%{
#include "ImdForce.h"
#include "OpenMM.h"
#include "OpenMMAmoeba.h"
#include "OpenMMDrude.h"
#include "openmm/RPMDIntegrator.h"
#include "openmm/RPMDMonteCarloBarostat.h"
%}

%pythoncode %{
import simtk.openmm as mm
%}

namespace ImdPlugin{

        class ImdForce : public OpenMM::Force {
            public:
            /**
             * Create a ImdForce.
             *
             * @param address    the IMD address.
             * @param port  the IMD port.
             * @param rate   the rate, in steps, at which IMD positions are sent.
             * @param useInternalUnits  whether to use internal OpenMM units or VMD units for transmission.
             */
            ImdForce(
            const std::string
            &address = "localhost",
            int port = 54555,
            bool waitForConnection = false,
            int rate = 10,
            bool useInternalUnits = false);
            /**
             * Get the IMD address.
             */
            const std::string
            &getAddress() const;
            /**
             * Get the IMD port.
             */
            const int getPort() const;

            /**
             * Whether the IMD server will wait for a connection.
             * @return
             */
            const bool willWaitForConnection() const;

            /**
             * The rate, in steps, at which IMD positions are transmitted.
             * @return
             */
            const int getRate() const;

            /**
             * Whether the IMD server will wait receive and transmit using OpenMM's internal units, or VMD.
             * @return
             */
            const bool usesInternalUnits() const;

            /**
             * Returns true if the force uses periodic boundary conditions and false otherwise. Your force should implement this
             * method appropriately to ensure that `System.usesPeriodicBoundaryConditions()` works for all systems containing
             * your force.
             */
            bool usesPeriodicBoundaryConditions() const;

        };
}
