//
// Created by jonathan on 17/12/2019.
//

#ifndef OPENMMIMD_DEBUG_H
#define OPENMMIMD_DEBUG_H
#ifndef NDEBUG // CMAKE uses not debug rather than debug.
#define IF_DEBUG if(1)
#else
#define IF_DEBUG if(0)
#endif
#endif //OPENMMIMD_DEBUG_H
