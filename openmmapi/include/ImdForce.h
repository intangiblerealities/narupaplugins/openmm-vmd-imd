#ifndef OPENMM_IMDFORCE_H_
#define OPENMM_IMDFORCE_H_

/* -------------------------------------------------------------------------- *
 *                                   OpenMM                                   *
 * -------------------------------------------------------------------------- *
 * This is part of the OpenMM molecular simulation toolkit originating from   *
 * Simbios, the NIH National Center for Physics-Based Simulation of           *
 * Biological Structures at Stanford, funded under the NIH Roadmap for        *
 * Medical Research, grant U54 GM072970. See https://simtk.org.               *
 *                                                                            *
 * Portions copyright (c) 2016 Stanford University and the Authors.           *
 * Authors: Peter Eastman                                                     *
 * Contributors:                                                              *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation files (the "Software"), *
 * to deal in the Software without restriction, including without limitation  *
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,   *
 * and/or sell copies of the Software, and to permit persons to whom the      *
 * Software is furnished to do so, subject to the following conditions:       *
 *                                                                            *
 * The above copyright notice and this permission notice shall be included in *
 * all copies or substantial portions of the Software.                        *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,    *
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR      *
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE  *
 * USE OR OTHER DEALINGS IN THE SOFTWARE.                                     *
 * -------------------------------------------------------------------------- */

#include "openmm/Context.h"
#include "openmm/Force.h"
#include <string>
#include <imd/ImdConnection.h>
#include "internal/windowsExportImd.h"

namespace ImdPlugin {

/**
 * This class implements a connection between OpenMM and IMD.  It is a Force object that you
 * add to the System with addForce().
 *
 */

class OPENMM_EXPORT_IMD ImdForce : public OpenMM::Force {
public:
    /**
     * Create a ImdForce.
     *
     * @param address    the IMD address.
     * @param port  the IMD port.
     * @param waitForConnection whether to wait for a connection before running dynamics.
     * @param rate   the rate, in steps, at which IMD positions are sent.
     * @param useInternalUnits  whether to use internal OpenMM units or VMD units for transmission.
     */
    ImdForce(const std::string& address="localhost",
            int port=54555,
            bool waitForConnection=false,
            int rate=10,
            bool useInternalUnits = false);
    /**
     * Get the IMD address.
     * @return The IPv4 address of the server.
     */
    const std::string& getAddress() const;
    /**
     * Get the IMD port.
     * @return The port to be used by the server.
     */
    const int getPort() const;

    /**
     * Whether the IMD server will wait for a connection.
     * @return True if the server will wait for a connection, false otherwise.
     */
    const bool willWaitForConnection() const;

    /**
     * The rate, in steps, at which IMD positions are transmitted.
     * @return The rate, in steps.
     */
    const int getRate() const;

    /**
     * Whether the IMD server will receive and transmit using OpenMM's internal units, or VMD.
     * @return True if using OpenMM's units, or VMD's units.
     */
    const bool usesInternalUnits() const;

    /**
     * Returns true if the force uses periodic boundary conditions and false otherwise. Your force should implement this
     * method appropriately to ensure that `System.usesPeriodicBoundaryConditions()` works for all systems containing
     * your force.
     */
    bool usesPeriodicBoundaryConditions() const {
        return false;
    }
protected:
    OpenMM::ForceImpl* createImpl() const;
private:
    bool waitForConnection;
    const std::string address;
    const int port;
    int rate;
    bool useInternalUnits;
};

} // namespace ImdPlugin

#endif /*OPENMM_IMDFORCE_H_*/
