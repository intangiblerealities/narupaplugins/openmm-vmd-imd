#include "openmm/Units.h"
#include <cstring>
#include "openmm/reference/ReferencePlatform.h"
#include "openmm/reference/RealVec.h"
#include "openmm/internal/ContextImpl.h"
#include "openmm/NonbondedForce.h"
#include "openmm/OpenMMException.h"
#include "ImdForce.h"
#include "ImdKernels.h"
#include <iostream>
#include <debug.h>


void ImdPlugin::CalcImdForceKernel::setupUnitConversions(const bool useInternalUnits)
{
    internalUnits = useInternalUnits;
    if(!internalUnits)
    {
        positionConversion = OpenMM::AngstromsPerNm;
        forceConversion = OpenMM::KJPerKcal / OpenMM::NmPerAngstrom;
    }
}

void ImdPlugin::CalcImdForceKernel::updatePositionsToSend(const std::vector<OpenMM::Vec3>& positions)
{
    for(int i=0; i < positions.size(); i++)
    {
        positionsToSend[i * 3 + 0] = (float) positions[i][0] * positionConversion;
        positionsToSend[i * 3 + 1] = (float) positions[i][1] * positionConversion;
        positionsToSend[i * 3 + 2] = (float) positions[i][2] * positionConversion;
    }
}

namespace ImdPlugin {

    CalcImdForceKernel::CalcImdForceKernel(std::string name, const OpenMM::Platform& platform) :
        OpenMM::KernelImpl(name, platform),
        paused(false),
        waitForConnection(false),
        rate(1),
        internalUnits(false),
        positionConversion(1.0),
        forceConversion(1.0)
    {
    }

    void CalcImdForceKernel::initializeImd(const OpenMM::System& system, const ImdForce& force)
    {
        // Construct and initialize the IMD interface object.
        imd.initialiseServer(force.getAddress(), force.getPort());
        paused = false;
        waitForConnection = force.willWaitForConnection();
        hasInitialized = true;
        rate = force.getRate();
        setupUnitConversions(force.usesInternalUnits());
        positionsToSend.resize(system.getNumParticles() * 3);
    }

    bool CalcImdForceKernel::isPaused()
    {
        IF_DEBUG std::cerr << "IMD Debug: Paused " << paused << std::endl;
        return paused;
    }


    IMDType CalcImdForceKernel::processImdInput() {
        IMDType messageType;
        if(!imd.isClientConnected())
            return IMDType::IMD_DISCONNECT;
        do
        {
            auto [receivedMessage, type, payload] = imd.receiveHeader(0, 0);
            messageType = type;
            // If we haven't received a message, and not paused, break out of the loop.
            if (!receivedMessage)
            {
                if(isPaused())
                    continue;
                else
                    break;
            }
            IF_DEBUG std::cerr << "IMD Debug: Received message " << messageType << std::endl;
            processImdMessage(messageType, payload);

        } while(imd.isClientConnected() && isPaused());
        return messageType;
    }

    void CalcImdForceKernel::processImdMessage(IMDType messageType, int payload) {

        switch (messageType) {
            case IMD_PAUSE:
                paused = true;
                std::cout << "Received pause command, MD pausing! " << messageType << std::endl;
                break;
            case IMD_GO:
                paused = false;
                std::cout << "Received go command, MD resuming! " << messageType << std::endl;

                break;
            case IMD_DISCONNECT:
                std::cout << "Client disconnecting!" << std::endl;
                imd.disconnectClient();
                break;
            case IMD_KILL:
                throw std::runtime_error("Received kill signal from interactive MD client.");
            case IMD_TRATE:
                std::cerr << "Setting IMD rate" << payload << std::endl;
                setRate(payload);
                break;
            case IMD_MDCOMM:
                receiveForces(payload);
                break;
            default:
                std::cerr << "Received unexpected message type from client " << messageType << std::endl;
                break;
        }
    }

    void CalcImdForceKernel::clearReceiveArrays()
    {
        std::fill(receivedAtomIndices.begin(), receivedAtomIndices.end(), 0);
        std::fill(receivedForces.begin(), receivedForces.end(), 0.0f);
    }

    void CalcImdForceKernel::receiveForces(int numForces)
    {
        if(receivedAtomIndices.size() < numForces)
        {
            receivedAtomIndices.resize(numForces);
            receivedForces.resize(numForces * 3);
        }
        clearReceiveArrays();

        imd.receiveMDCommunication(numForces, receivedAtomIndices, receivedForces);
    }

    void CalcImdForceKernel::setRate(int newRate)
    {
        if(newRate > 0)
            rate = newRate;
    }

    void CalcImdForceKernel::sendPositions()
    {
        imd.sendPositions(positionsToSend);
    }
}
