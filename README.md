# OpenMM VMD IMD API Plugin

This project implements the VMD IMD API in [OpenMM](http://openmm.org),
enabling interactive molecular dynamics from clients that support it. 

It's intended to be used with [Narupa](https://gitlab.com/intangiblerealities/narupa-protocol), allowing use of all of OpenMM's integrators
and better performance (instead of the ASE integrator used for small systems). 


## Building The Plugin

Prequisites: 

* OpenMM
* SWIG 
* CUDA and OpenCL, if desired. 
* Python, if the wrapper is desired. 
* CMake
* A C++17 Compiler (e.g. Visual Studio on Windows, gcc on Linux, clang on Mac).


This project uses [CMake](http://www.cmake.org) for its build system.  To build it, follow these
steps:

1. Create a directory in which to build the plugin.

2. Run the CMake GUI or ccmake, specifying your new directory as the build directory and the top
level directory of this project as the source directory.

3. Press "Configure".

4. Set OPENMM_DIR to point to the directory where OpenMM is installed.  This is needed to locate
the OpenMM header files and libraries.

6. Set CMAKE_INSTALL_PREFIX to the directory where the plugin should be installed.  Usually,
this will be the same as OPENMM_DIR, so the plugin will be added to your OpenMM installation.

7. If you plan to build the OpenCL platform, make sure that OPENCL_INCLUDE_DIR and
OPENCL_LIBRARY are set correctly, and that IMD_BUILD_OPENCL_LIB is selected.

8. If you plan to build the CUDA platform, make sure that CUDA_TOOLKIT_ROOT_DIR is set correctly,
and that IMD_BUILD_OPENCL_LIB is selected.

9. On Linux, it may be necessary to use the older C++ ABI (see [this issue](https://github.com/pandegroup/openmm-nn/issues/8)). 
This can be configured by setting USE_OLD_CXX11_ABI to `on`.

10. Press "Configure" again if necessary, then press "Generate".

11. Use the build system you selected to build and install the plugin.  For example, if you
selected Unix Makefiles, type `make install` to install the plugin, and `make PythonInstall` to
install the Python wrapper. On Windows, click `Open Project` in CMake, which will open 
Visual Studio. Right click BUILD_ALL, click Build. Repeat for INSTALL and PYTHON INSTALL.

## Using The Plugin


Create an `ImdForce` object, and add it to your [OpenMM system](http://docs.openmm.org):

``` python
import openmmimd
import simtk.openmm as mm

# Optionally, customise the parameters. 

address = 'localhost'  # what address to run the IMD on.
port = 9000  # port run the IMD server on.
wait = True  # whether to wait for a connection before running MD steps
rate = 20  # rate at which to send molecular dynamics positions to the client
useInternalUnits = False  # whether to output with OpenMM's internal units, or VMD units (angstroms, kCal/mol).

imdForce = openmmimd.ImdForce(address, port, wait, rate, useInternalUnits)
system: mm.System # OpenMM system that you've created. 
system.addForce(imdForce)

```

That's it! With that in place, you'll be able to connect to it from VMD or Narupa.


## License

This was largely based on the OpenMM [Example Plugin](https://github.com/peastman/openmmexampleplugin) 
and [VMD IMD API](https://www.ks.uiuc.edu/Research/vmd/imd/), and is distributed under the licenses below.

All additional contributions are distributed under the [MIT license](LICENSE), (c) Intangible Realities
Laboratory, University of Bristol.


### OpenMM  

This is part of the OpenMM molecular simulation toolkit originating from
Simbios, the NIH National Center for Physics-Based Simulation of
Biological Structures at Stanford, funded under the NIH Roadmap for
Medical Research, grant U54 GM072970. See https://simtk.org.

Portions copyright (c) 2016 Stanford University and the Authors.

Authors: Peter Eastman

Contributors:

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS, CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
USE OR OTHER DEALINGS IN THE SOFTWARE.

### VMD 

University of Illinois Open Source License
Copyright 2006 Theoretical and Computational Biophysics Group,
All rights reserved.

Developed by:		Theoretical and Computational Biophysics Group
			University of Illinois at Urbana-Champaign
			http://www.ks.uiuc.edu/

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the Software), to deal with
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to
do so, subject to the following conditions:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimers.

Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimers in the documentation
and/or other materials provided with the distribution.

Neither the names of Theoretical and Computational Biophysics Group,
University of Illinois at Urbana-Champaign, nor the names of its contributors
may be used to endorse or promote products derived from this Software without
specific prior written permission.

THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS WITH THE SOFTWARE.
