# Requires Visual Studio Build Tools and cmake to be installed, and configured
# e.g., the following in a bat file: 
# CALL "C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\Tools\VsDevCmd.bat" %*
# start powershell

# Required variables

#$OPENMM_INSTALL_DIR
#$PLUGIN_SRC_DIR
#$PLUGIN_BUILD_DIR
#$PLUGIN_INSTALL_DIR
#$PYTHON_EXECUTABLE
#$SWIG_EXECUTABLE

mkdir $PLUGIN_BUILD_DIR
cd $PLUGIN_BUILD_DIR
cmake -G "Visual Studio 15 2017" -A x64 $PLUGIN_SRC_DIR `
    $('-DOPENMM_DIR='+$OPENMM_INSTALL_DIR) `
    -DCMAKE_BUILD_TYPE=Release `
    $('-DCMAKE_INSTALL_PREFIX='+$PLUGIN_INSTALL_DIR) `
    $('-DPYTHON_EXECUTABLE='+$PYTHON_EXECUTABLE) `
    $('-DSWIG_EXECUTABLE='+$SWIG_EXECUTABLE) `

cmake --build . --config Release
cmake --build . --target install --config Release

cmake --build . --target PythonInstall --config Release